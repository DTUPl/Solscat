import numpy as np
import matplotlib.pyplot as plt

class Simulate_scattering_image:
    '''
    This class performs a complete scattering simulation for a Liquid scattering experiment
    
    Parameters:
        exp_info.py: python file
            contains scattering experiment information
    '''
    def __init__(self, energy=None, distance_to_detector = None):        
        from exp_info import x_det_length, y_det_length, no_of_pixels#, energy, distance_to_detector 
        from exp_info import detector_thickness, detector_mu, jet_thickness, jet_mu
        from md_info import solvent_stoich

        if energy is None:
            from exp_info import energy
        if distance_to_detector is None:
            from exp_info import distance_to_detector
        
        self.x_det_length = x_det_length
        self.y_det_length = y_det_length
        self.no_of_pixels = no_of_pixels
        self.energy = energy
        self.L0 = distance_to_detector
        self.noofpixelscalc = no_of_pixels**0.5
        self.centerpx = (self.noofpixelscalc+1)/2
        self.PixelSize = x_det_length/self.noofpixelscalc
        
        self.q = np.arange(0.1, 14, 0.05)
        
        self.xdet, self.ydet = np.meshgrid(np.arange(self.noofpixelscalc), np.arange(self.noofpixelscalc))
        self.xdet = (self.xdet-self.centerpx)*self.PixelSize
        self.ydet = (self.ydet-self.centerpx)*self.PixelSize
        
        # fixed detector parameters
        self.detector_thickness = detector_thickness #40e-6
        self.detector_mu = detector_mu #20.5 * 1000 #18keV Value!
        
        # Absorption of water Jet
        #self.jet_thickness = 300e-6
        self.jet_thickness = jet_thickness #360e-6 # comparison thesis
        self.jet_mu = jet_mu #8.096e-01 * 0.997 * 100 # Value for 20keV
        
        sample_signal = np.loadtxt("simulated_scattering.txt").T
        self.signal = np.interp(self.q, sample_signal[0], sample_signal[1])

        # calculate incoherent scattering 
        atom_list = []
        a=0
        for entry in solvent_stoich:
            for i in range(entry[0]):
                atom_list.append([entry[1],a,0,0])
                a+=1
        self.signal += calculate_scattering_inc(atom_list, self.q)
        self.calculate_pixel_specific_parameters()

        self.signal = np.interp(self.q_matrix, self.q, self.signal)

       
    def apply_air_scattering(self, n_steps = 50):
        '''
            Calculates the air_scattering and adds it on top of the solvent signal
            The jet or its absorption is not included in estimations
            
            Parameters:
                min_dis: float
                    distance of the beampipe from the detector [m]
                max_dis:
                    distance of the beamstop from the detector [m]
                n_steps:
                    simulation steps
                    
            output: adds self.gas_signal and self.gas_molecules_scattered to the class
        '''
        
        from exp_info import density_gas, molar_mass_gas, min_distance, max_distance, beam_area

        N_avo = 6.02214179e23
        Lambda=12.398/self.energy
        angles = np.arcsin( self.q * Lambda /(4*np.pi))
        
        air_scattering = np.loadtxt("tools/air_scattering.txt").T
        self.air_scattering = np.interp(self.q, air_scattering [0], air_scattering [1])
        
        total_air_scattering = np.zeros(len(self.q))        
        for L0_c in np.linspace(min_distance, max_distance, n_steps):
            xy_dis_on_detector = np.tan(angles) * L0_c 
            angles_experiment = np.arctan(xy_dis_on_detector/self.L0)
            q_range_experiment = 4*np.pi*np.sin(angles_experiment)/Lambda    
            scat_interp = np.interp(self.q, q_range_experiment, self.air_scattering)      
            total_air_scattering += scat_interp /n_steps

        self.gas_signal = np.interp(self.q_matrix, self.q,total_air_scattering)
        self.gas_molecules_scattered = beam_area * ((max_distance-min_distance) ) * density_gas / molar_mass_gas * N_avo  


        
    def calculate_pixel_specific_parameters(self, bcx=0, bcy=0):
        '''
        This method calculates all pixel specific parameters
        such as distance, q, and corrections
        WARNING: may take time to compute for larger pixel numbers
        '''
        
        wavelength =12.398/self.energy
        
        detx_rel = self.xdet - bcx
        dety_rel = self.ydet - bcy       
        
        pol = 0.05
        
        self.distance = (detx_rel**2 + dety_rel**2 + self.L0**2)**0.5
        self.theta = np.arccos(self.L0/self.distance)
        self.q_matrix = 4 * np.pi/wavelength * np.sin(self.theta/2)
        
        sint = np.sqrt((detx_rel**2 + dety_rel**2)/self.distance)
        sinp = dety_rel / np.sqrt(detx_rel**2 + dety_rel**2)
        cosp = detx_rel / np.sqrt(detx_rel**2 + dety_rel**2)        
        self.cor_pol =  1/(pol * (1-(sinp*sint)**2) + (1-pol)* (1-(cosp*sint)**2))
        
        self.cor_geo = (self.L0**2/self.distance**2) * self.L0/self.distance
        #self.cor_geo = 1 #inside delOmeg
        self.absorption_detector = 1-np.exp(-self.detector_mu * self.detector_thickness / np.cos(self.theta))
        self.absorption_jet = 1 - 1/(self.jet_mu * self.jet_thickness) * (np.cos(self.theta)/(1-np.cos(self.theta))) * (
                    np.exp(-self.jet_mu*self.jet_thickness) - np.exp(-self.jet_mu*self.jet_thickness/np.cos(self.theta)))
        
        self.delOmeg = np.cos(self.theta)**3/self.L0**2 * self.PixelSize**2
        #delOmeg = (PixelSize/DistanceToDetector * cosd(TwoThetaMatrix)).^2;

        #self.delOmeg = (self.PixelSize/self.L0 * np.cos(self.theta))**2
        return
        
        
        
    def scatter_photons_sample(self, n_photons=1e9,n_pulses=1):
        '''
            Simulates the 2D scattering signal of self.signal given n_photons & n_pulses parameters        
        '''
        from exp_info import beam_area, sample_density, sample_molar_mass
        eps0 = 8.854187817e-12
        me = 9.1093821545e-31
        e = 1.60217648740e-19
        c_light=2.998e8
        N_avo = 6.02214179e23
        r0_thomson = e**2/(4*np.pi*eps0*me*c_light**2)

        N_avo = 6.02214179e23
        density_water = sample_density #997 # kg/m3
        molar_mass_water = sample_molar_mass  #0.018 #kg/Mol
        water_molecules_scattered = beam_area* self.jet_thickness * density_water / molar_mass_water * N_avo
        
        scattering_strength = n_photons * (r0_thomson)**2* water_molecules_scattered * n_pulses /beam_area       
        Intensity = scattering_strength* self.signal *self.delOmeg #/ self.cor_geo

        if hasattr(self, 'gas_signal'):
            print("water_molecules_scattered /1e15: ", water_molecules_scattered /1e15)
            print("gas_molecules_scattered: /1e15", self.gas_molecules_scattered/1e15)
            scattering_strength = n_photons * (r0_thomson)**2* self.gas_molecules_scattered * n_pulses  /beam_area
            Intensity += scattering_strength* self.gas_signal *self.delOmeg #/ self.cor_geo

        # Apply corrections
        Intensity = Intensity / self.cor_pol * self.absorption_detector * (1-self.absorption_jet)
        return Intensity
    
    def correct_intensity(self,image, flag = 'in'):
        '''
        Applies corrections to a passed detector image
        
        Parameters:
            image : n*m detector matrix
            flat : in = turn correct signal into measured signal, out = turn measured signal into correct signal
        '''
        #return image / self.cor_geo
        return image * self.cor_pol / self.cor_geo / self.absorption_detector / (1-self.absorption_jet)


        if flag == "in":
            return image/self.cor_pol *  self.cor_geo * self.absorption_detector *  (1-self.absorption_jet)# *self.delOmeg
        if flag == "out":
            return image * self.cor_pol / self.cor_geo / self.absorption_detector / (1-self.absorption_jet)# /self.delOmeg
    
    def simulate_scattering_signal(self, n_pulses = 1, n_photons = 1e9):
        '''
            Simulates 1D Scattering image
            
            Paramters:
            n_pulses : int
                number of pulses
            n_phoyons : in
                number of photons per pulse
            
            Returns:
            qvector, 1D scattering signal
            
        '''
        image = self.scatter_photons_sample(n_photons=n_photons, n_pulses = n_pulses)
        return self.integrate_image(image)
        return self.integrate_image(self.correct_intensity(image)) 
        
    def integrate_image(self, image, n_q_bins = 800):
        '''
        Integrates a 2D images into a 1d signal with n_q_bins q bins
        
        Paramters:
            image : n*m detector matrix
            n_q_bins: number of q bins
        '''
        q_axis = np.arange(np.min(self.q_matrix), np.max(self.q_matrix), (np.max(self.q_matrix)- np.min(self.q_matrix))/n_q_bins)
        I_axis = np.zeros(len(q_axis))
        for i in range(len(q_axis)-1):
            ring = image[(self.q_matrix> q_axis[i]) & (self.q_matrix < q_axis[i+1])]
            I_axis[i] = np.nanmean(ring)
        return q_axis, I_axis


def calculate_scattering(atom_list, q_vector):
    '''
    This function calculates the total scattering signal of a structure for a specified q vector
    '''
    #if not calculate_scattering.cm_coeffs in locals():
    if not hasattr(calculate_scattering, 'cm_coeffs'):
        calculate_scattering.cm_coeffs = generate_cromermann_coeffs() # saved in persistent variable
    f0_List = list(map(calculate_form_factor_atom, atom_list, [q_vector] * len(atom_list), [calculate_scattering.cm_coeffs] * len(atom_list)))
    favg = 0
    for x in range(0, len(atom_list)):
        favg += f0_List[x] * np.conj(f0_List[x])
        atom_1 = atom_list[x]
        for a in range(x + 1, len(atom_list)):
            atom_2 = atom_list[a]
            q_rnm = (q_vector) * np.linalg.norm(
                np.asarray([atom_1[1], atom_1[2], atom_1[3]]) -
                np.asarray([atom_2[1], atom_2[2], atom_2[3]]))
            favg += 2 * f0_List[x] * f0_List[a] * (np.sin(q_rnm) / q_rnm)
    return favg 


def calculate_scattering_inc(atom_list, q_vector):
    '''
    This function calculates the total scattering signal of a structure for a specified q vector

    Parameters:
        atom_list: list of lists
            Form: [['Element1',x,y,z], ['Element2',x,y,z], ...]
        q_vector: array in Å-1

    Output:
        1D Array containing simulated incoherent scattering
    '''
    #if not calculate_scattering.cm_coeffs in locals():
    if not hasattr(calculate_scattering, 'cm_coeffs'):
        calculate_scattering.cm_coeffs = generate_cromermann_coeffs() # saved in persistent variable
    #f0_List = list(map(calculate_form_factor_atom, atom_list, [q_vector] * len(atom_list), [calculate_scattering.cm_coeffs] * len(atom_list)))
    inc_parameter_list = make_haidu_list(atom_list)
    print("lengths: ", len(inc_parameter_list), len(atom_list))
    total_inc_scattering = np.zeros(len(q_vector))
    
    s=(q_vector/(4*np.pi))
    s = q_vector
    f0_List = list(map(calculate_form_factor_atom, atom_list, [s] * len(atom_list), [calculate_scattering.cm_coeffs] * len(atom_list)))
    
    for x in range(0, len(atom_list)):
        # takes the coherent scattering
        favg = f0_List[x] * np.conj(f0_List[x])
        atom_1 = atom_list[x]
        y = x
        if inc_parameter_list[y][0] == 'Haidu':
            Z  = inc_parameter_list[y][2]
            M  = inc_parameter_list[y][3]
            K  = inc_parameter_list[y][4]
            L  = inc_parameter_list[y][5]
            I_incoherent = (Z - favg/Z) * (1-M * (np.exp(-K*s)- np.exp(-L*s)  )  )
        elif inc_parameter_list[y][0] == 'Palinka':
            #print(inc_parameter_list[a], a)
            #print(inc_parameter_list[a][5])

            Z  = inc_parameter_list[y][2]
            a  = inc_parameter_list[y][3]
            b  = inc_parameter_list[y][4]
            c  = inc_parameter_list[y][5]

            w = 0.176/(Z**(2/3)) * q_vector
            I_incoherent = (1 - (a/(1+b*s)**c))

            #I_incoherent = Z * (1 - (a/(1+b*s)**c))
        elif inc_parameter_list[y][0] == 'Hydrogen':
            I_incoherent = np.zeros(len(q_vector))                
        else:
            raise ValueError("Warning:")
        total_inc_scattering += I_incoherent
            
    return total_inc_scattering


def generate_cromermann_coeffs():
    '''
    reads in the CromerMannCoeffs

    returns: CM Parameters
    '''
    f = open("tools/f0_CromerMann.txt", "r")
    Parameters = []
    for line in f:
        if line[:2] == "#S":
            Entry = []
            line = line.split()
            Entry.append(line[-1])
            x=next(f)
            x=next(f)
            x=next(f)
            x = x[:-2].split()
            Entry = [*Entry, *x]
            Parameters.append(Entry)
    return Parameters

def calculate_form_factor_atom(Atom, Qvector, CMCoeffs):
    '''
    This function calculates the structure factors for a give atom list
    Possible optimization: one call per atom type
    '''
    atom = GetFormFactorParameters(Atom[0], CMCoeffs)
    f0 = atom[4]
    q2 = (Qvector / (4 * np.pi)) ** 2
    for x in range(0, 4):
        f0 = f0 + atom[x] * np.exp(-atom[x + 5] * q2)
    return f0

def GetFormFactorParameters(AtomName,Parameters):
    '''
    Reads out and returns the form factor parameters for atoms
    '''
    for entry in Parameters:
        if AtomName == entry[0]:
            coeffs = entry[1:]
            coeffs = np.array(coeffs,dtype=float) # Turns the parameters read as string into floats
            return coeffs
    raise ValueError("Element "+ str(AtomName) +" not in Database")

def make_haidu_list(atom_list):
    '''
    Reads out coefficients for incoherent scattering

    Parameters:
        atom_list: list of lists
            Form: [['Element1',x,y,z], ['Element2',x,y,z], ...]

    Output:
        list of lists containing the parameters neccessary for calculating incoherent scattering    
    '''
    HDCoefficients = GenerateHaiduCoeffs()
    PLCoefficients = GeneratePalinkaCoeffs()
    list_to_return = []
    for i in range(len(atom_list)):
        found = False
        entry = atom_list[i]
        for info in HDCoefficients:
            if entry[0] == info[0]:
                list_to_return.append(['Haidu', *info])
                found = True
                break
        for info in PLCoefficients:
            if entry[0] == info[0]:
                list_to_return.append(['Palinka', *info])
                found = True
                break
        if not found:
            list_to_return.append(['Hydrogen'])
            #print("Could not find: ", entry[0])
    return list_to_return

def GenerateHaiduCoeffs():
    f = open('tools/HaiduCoefficients.txt','r')
    returnList = []
    next(f)
    next(f)
    for line in f:
        Curr_Line = []
        linesplitted = line.split()
        Curr_Line.append(linesplitted[0])
        for entry in linesplitted[1:5]:
            Curr_Line.append(float(entry))
        returnList.append(Curr_Line)
    return returnList

def GeneratePalinkaCoeffs():
    f = open('tools/PalinkaCoefficients.txt', 'r')
    returnList = []
    for line in f:
        Curr_Line = []
        linesplitted = line.split()
        Curr_Line.append(linesplitted[0])
        for entry in linesplitted[1:5]:
            Curr_Line.append(float(entry))
        returnList.append(Curr_Line)
    return returnList


def simulate_signal(solvent = "H20", energy=18, distance_to_detector = 0.05, n_photons = 1e9, n_pulses=1, energy_file = None):
    if solvent !="H20": 
        print("Warning: solvent not implemented")
        return
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    a = Simulate_scattering_image(energy = energy, distance_to_detector = distance_to_detector)
    #a.apply_air_scattering()
    detector_image = a.scatter_photons_sample(n_photons=n_photons, n_pulses=n_pulses)
    corrected_detector_image = a.correct_intensity(detector_image)

    if energy_file != None:
        final_detector_image = np.zeros(np.shape(detector_image))
        final_corrected_image = np.zeros(np.shape(detector_image))
        energy_spec = np.loadtxt(energy_file).T
        for energy_index in range(len(energy_spec[0])):
            b = Simulate_scattering_image(energy = energy_spec[0][energy_index], distance_to_detector = distance_to_detector)
            curr_det_image = b.scatter_photons_sample(n_photons=n_photons, n_pulses=n_pulses)* energy_spec[1][energy_index]
            final_detector_image += curr_det_image
            final_corrected_image += b.correct_intensity(curr_det_image)
        detector_image = final_detector_image/np.sum(energy_spec[1])
        corrected_detector_image = final_corrected_image/np.sum(energy_spec[1])

    fig, (ax1, ax2) = plt.subplots(1, 2)
    q, sig = a.integrate_image(detector_image)
    ax1.plot(q, sig,label="uncorrected Integrated")
    q, sig = a.integrate_image(corrected_detector_image)
    ax1.plot(q, sig,label="corrected Integrated")
    ax1.legend()
    im = ax2.pcolor(detector_image[::5,::5])
    divider = make_axes_locatable(ax2)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(im, cax=cax, orientation='vertical')
    ax1.set_ylabel("S(Q) [a.u.d.]")
    ax1.set_xlabel("Q [Å$^{-1}$]")
    ax2.set_ylabel("det y [pixel]")
    ax2.set_xlabel("det x [pixel]")
    ax1.set_title("Az integrated")
    ax2.set_title("Uncorrected 2D Detector image")
    plt.show()
    # save 1D image

def calculate_photons(solvent="H20", energy=18, distance_to_detector = 0.05, n_pulses=1, energy_file = None, peak_counts=1e4):
    if solvent !="H20": 
        print("Warning: solvent not implemented")
        return
    a = Simulate_scattering_image(energy = energy, distance_to_detector = distance_to_detector)
    #a.apply_air_scattering()
    detector_image = a.scatter_photons_sample(n_photons=1e9, n_pulses=n_pulses)
    max_counts = np.amax(detector_image)
    photons_estimated = peak_counts/max_counts * 1e9
    print("Estimated photons (/1e9): ", photons_estimated/1e9)
    #corrected_detector_image = a.correct_intensity(detector_image)


def calculate_photons_corrected(peak_counts,solvent="H20", energy=18,
      distance_to_detector = 0.05, n_pulses=1, phosphor_norm = True):
    '''
        estimates the amount of incident photons based on the peak value of the solvent ring

    Parameters:
        solvent : str, solvent from database
        energy : energy [keV]
        distance_to_detector : detector distance
        n_pulses : 
        phosphor_norm : is the phosphor correction normalized to the center of the detector 
    
    '''

    if solvent =="H20":
        a = Simulate_scattering_image(energy = energy, distance_to_detector = distance_to_detector)
        detector_image = a.scatter_photons_sample(n_photons=1e9, n_pulses=n_pulses)
        corrected_detector_image = a.correct_intensity(detector_image)
        max_counts = np.amax(corrected_detector_image)
        detector_index = np.where(corrected_detector_image==np.amax(corrected_detector_image))
        #print("detector_index", detector_index[0][0], detector_index[1][0])
        q_value = a.q_matrix[detector_index[0][0], detector_index[1][0]]
        #print(q_value, max_counts )
        photons_estimated = peak_counts/max_counts * 1e9
        if phosphor_norm:
            photons_estimated = photons_estimated / np.amin(a.absorption_detector)

        print("Estimated photons (/1e9): ", photons_estimated/1e9, " at a q value of:", round(q_value,2))
        return photons_estimated
    pass