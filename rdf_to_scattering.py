'''
	This script takes RDFs and information out of md_info.py
	to calculate a total scattering pattern

	Input:
	md_info.py
	RDFs in "RDFs/"

	output:
	simulated_scattering.txt

'''

from tools.sgr import SGr, Damping
from md_info import sim_vol, stoich_dict, n_solvent_molecules
import numpy as np

rdf_directory = "RDFs/"

damp = Damping(style='simple', L=sim_vol**(1/3)/2, r_max = 20)

s = SGr(volume=sim_vol, damp = damp,skip_missing = True)
s.set_stoichometry(stoich_dict)
s.load_rdfs_fromdir(rdf_directory)

out = s.calculate()

total_scattering_signal = out['s_v_damp']  / n_solvent_molecules
a = np.asarray([out['q'].T, total_scattering_signal.T ]).T
np.savetxt("simulated_scattering.txt",a, delimiter = "\t")



