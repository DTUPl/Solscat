'''
	Info file used to interpret the topology and trajectory data
	to generate the RDFs

	all file paths must lead to a mdanalysis readable format
	
	information
	--------------
	sim_vol : float
	    simulation volume of md in Å**3
	solvent_stoich : list of lists
	    contains information about the amount of atoms per solvent molecule
	topo_file : path
	    location of topology file of md simulation
	trajectory_file : path
	    location of the trajectore file of md simulation,
	solvent_groups: list of lists
	    identifier of solvent atoms, as specified by the topological file
	group_names: list of strings
	    name tages used for saving the rdfs to file after processing
	stoich_dict : dict
	    contains all kinds of atoms together with their numbers in the md simulation box
'''

sim_vol = 44.49*44*44.52

solvent_stoich = [[1,'O'],[2,'H']]

topo_file = 'md_data/agptpop_solv_custom.prmtop'

trajectory_file = "md_data/agptpop_gs_npt_gpu.dcd"

solvent_groups = [['name O'], ['name H1', 'name H2']]

group_names = ['O_v', 'H_v']

n_solvent_molecules = 3871

stoich_dict = {
    'Pt_u': 2,
    'P_u': 8,
    'O_u':20,
    'H_u':10,
    'Ag_u':1,
    'O_v': 2871,
    'H_v': 2871*2
}

