'''
	This script uses the md_info.py information to 
	sample the RDFs from the md simulation

	Input:
	md_info.py file in folder

	output:
	RDFs of solvent groups in the subfolder "RDFs"
'''
import MDAnalysis as mda
from MDAnalysis.analysis.rdf import InterRDF
import numpy as np
from md_info import sim_vol,solvent_stoich,topo_file,trajectory_file,solvent_groups,group_names

universe =  mda.Universe(topo_file, trajectory_file)

save_directory = 'RDFs/'

rdf_names = []
rdf_data = []

#for entry in solvent_groups:
for i in range(len(solvent_groups)):
    for j in range(i, len(solvent_groups)):
        print("--------")
        print("Compute RDF between:", i, j, solvent_groups[i],solvent_groups[j])
        #print(u.select_atoms(*solvent_groups[i]), u.select_atoms(*solvent_groups[j]))
        rdf = InterRDF(universe.select_atoms(*solvent_groups[i]), universe.select_atoms(*solvent_groups[j]), nbins=100, range=(0.1,15.0))
        rdf.run()
        rdf_names.append("g"+group_names[i] +"-" + group_names[j]+".dat")
        rdf_data.append([rdf.bins, rdf.rdf])


for i in range(len(rdf_names)):
    a = np.asarray(rdf_data[i][0:2]).T
    np.savetxt(save_directory + rdf_names[i],a, delimiter = "\t")
