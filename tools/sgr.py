import glob, os, re, itertools
import numpy as np
from ase import Atoms
from scipy.integrate import cumtrapz, trapz, simps
from scipy.optimize import minimize
#from .xpot import Debye  # for atomic_f0. TODO: put somewhere more central
#from cmmtools.cmm.xray.debye import Debye
from tools.debye import Debye

def integrate(r, g):
    return cumtrapz(r**2 * g, dx=r[1] - r[0])

def posdens(q, R):
    return (np.sin(q * R ) - q * R * np.cos(q * R)) / q**3

def find_nearest(array, value=0):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx, array[idx]

def w3d(r, R):
    '''2R is the integration limit, so half the box length. So max(r)/2.
    10.1021/jz301992u  '''
    x = r / (2 * R)
    w = 4 * np.pi * r**2 * (1 - 3 * x / 2 + x**3 / 2)
    return w

def w3d_extrap(r, R, alpha=1):
    x = r / (2 * R)
    return 4 * np.pi * r**2 * (1 - x**3) * alpha

class Damping:
    '''
    style: str
        can be either of these:
                'simple': sin(pi*L) / pi*L
                'dhabal': as in 10.1039/c6cp07599a
                'zederkof': as above but with an r_cut as well.
                'panman': Exponential fade function

    L: Float
        Damping factor for the simple damping, usually 1/2 of
        the box length.
    r_max: Float
        The r-value where the rdf should be 0
    r_cut: Float (only for zederkof damping)
        When to start the smooth cutoff. Everything before
        is untouched.

    '''
    def __init__(self, style, L=None, r_max=None, r_cut=None):
        self.style = style
        self.L = L
        self.r_max = r_max
        self.r_cut = r_cut

    def simple(self, R):
        damp = self.L
        return np.sin(np.pi * R / damp) / (np.pi * R / damp)

    def panman(self, R):
        fade = self.L
        mask = R > fade
        damping = np.ones(len(R))
        damping[mask] = np.exp(-(R[mask] / fade - 1)**2)
        return damping

    def dhabal(self, R):
        r_max = self.r_max
        fac = (1 - 3*(R/r_max)**2) * (R <= r_max/3) +\
               3 / 2 * (1-2*(R/r_max) +\
               (R/r_max)**2) * ((r_max / 3 < R) & (r_max > R))

        return fac

    def zederkof(self, R):
        r_max = self.r_max
        r_cut = self.r_cut

        fac = 1 * (R < r_cut) +\
              (1 - 3 * ((R - r_cut) / (r_max - r_cut))**2) * ((R <= 2 * r_cut / 3 + r_max / 3) & (R > r_cut)) +\
              3 / 2 * (1 - 2 * ((R - r_cut) / (r_max - r_cut)) +
              (( R - r_cut) / (r_max - r_cut))**2) * ((R > 2 * r_cut / 3 + r_max / 3) & (r_max > R))
        return fac

    def damp(self, R):
        if self.style == 'simple':
            damping = self.simple
        elif self.style == 'dhabal':
            damping = self.dhabal
        elif self.style == 'zederkof':
            damping = self.zederkof
        elif self.style == 'panman':
            damping = self.panman
        else:
            raise RuntimeError(f'Damping style not understood: {self.style}')

        return damping(R)



class SGr:
    ''' X-Ray Scattering from pairwise radial distribution functions.

        Can divide signal up in Solute-Solute (u-u), Solute-Solvent (u-v)
        and Solvent-Solvent (v-v) contributions.

        See e.g.: DOI: 10.1088/0953-4075/48/24/244010


        The rdf-loader takes in .dat files in the following format:

            gX_a-Y_b.dat

        where X,Y is the atom type, and a,b is either u or v, for solUte
        and solVent, respectively.

        Parameters:

            volume: Float
                volume of the simulation cell
            damp: str / Extender()
                either any of the 'style'-inputs for the Damping class above
                or
                an Extender() object
            ignore_elements: list of str
                elements to ignore when looking for rdf files. E.g.
                to avoid including scattering from counterions even
                though they are still present in the .xyz file from
                which the stoichometry is calculated.


        Uses ASE to read xyz files for the stoichometry. Kinda overkill,
        if you for some reason don't want to install the package, use your
        own xyz reader, or simply pass in a dict {'Element': Number, ...}
        to the set_stoichometry() method.

        Example:
        >>> dir_with_rdfs = '/some/location/'
        >>> sgr = SGr(volume=50**3, damp=25, ignore_elements=['Cl'] )
        >>> idx = 61  # if you have 61 atoms in your solute
        >>> sgr.set_stoichometry('single_frame_of_cell.xyz', u_idx=idx)
        >>> output = sgr.calculate(dir_with_rdfs)
        '''

    def __init__(self, volume, qvec=np.arange(0.0001, 10, 0.01), damp=None,
                 ignore_elements=[], delta=False, verbose=True, skip_missing=False):
        self.qvec = qvec
        self.v = volume
        self.ie = ignore_elements  # e.g. counterions
        self.f0 = None
        self.stoich = None
        self.alpha_dv = None
        self.all_names = None  # rdf generated from stoich for book-keeping
        self.out = None
        self.delta = delta  # True: eqn 22 is used, so dRDFs and no first term
        self.verbose = verbose
        self.vegt = False
        self.skip_missing = skip_missing

        self.r_max = None  # Cut the RDF off here
        self.r_avg = None  # use the avg of the RDF at r > r_avg instead of 1 in g(r) - 1

        self.db = Debye(qvec)

        self.damp = damp

    def calculate(self, arg=None, stoich=None, avg_dv=False, ptablef0=False):
        ''' Load rdfs from files
            Figure out which atom types are in the system
            Figure out to which contribution they belong
            Get number of atom types either manually or from some other source
            Get CM parameters for these atom types
            Update an atomic form factor library
            Calculate first term
            Calculate second term.
            Format the output and output '''
        if arg is None:
            pass
        elif isinstance(arg, str):  # assume its path to dir
            self.load_rdfs_fromdir(arg)
        else:
            self.rdfs = arg

        if str(self.damp) == 'Extender':
            self.extend_and_fit()

        self.set_stoichometry(stoich)

        if ptablef0:
            self.atomic_f0_pt(self.stoich)
        else:
            self.atomic_f0(self.stoich)

        self.bookkeep()

        s_u = np.zeros(len(self.qvec))
        s_v = np.zeros(len(self.qvec))
        s_t = np.zeros(len(self.qvec))

        s_vdv = np.zeros(len(self.qvec))
        s_vdv_damp = np.zeros(len(self.qvec))
        if avg_dv:
           self.calc_avg_fdv()
        else:
            self.calc_fdv()  # for SAXS-style solvent term

        #if not self.delta:
        s_u, s_v, s_t, s_vdv, s_vdv_damp, s_vdv_kb = self.first_term()
        if self.delta:  # first dv terms are still needed in deltas
            s_u *= 0
            s_v *= 0
            s_t *= 0

        out = self.second_term(s_u, s_v, s_t, s_vdv, s_vdv_damp, s_vdv_kb)

        return out

    def extend_and_fit(self):
        new_rdfs = []
        for rdf in self.rdfs:
            ext = self.damp
            ext.rdf = np.column_stack((rdf['r'], rdf['gr']))  # Consistent data structures? nooo, who wants that.
            ext.replace()
            ext.fit()
            rdf['r'] = ext.rdf_long[:, 0]
            rdf['gr'] = ext.rdf_long[:, 1]
            rdf['fitted_damp'] = ext.fit_damp
            rdf['extender'] = ext
            new_rdfs.append(rdf)
        self.rdfs = new_rdfs


    def load_rdfs_fromdir(self, directory, selection=None):
        files = sorted(glob.glob(os.path.join(directory, 'g*.dat')))
        if selection is not None:
            files = sorted(glob.glob(os.path.join(directory, selection)))

        rdfs = []
        for f in files:
            name = f.split(os.sep)[-1]  # strip the directory path away

            # use reg exp since atom types can be 1 or 2 chars long:
            atyp1 = re.search('g(.*?)_', name).group(1)
            part1 = re.search(atyp1 + '_(.*?)-', name).group(1)

            atyp2 = re.search('-(.*?)_', name).group(1)
            part2 = re.search('-' +  atyp2 + '_(.*).dat', name).group(1)

            if (atyp1 in self.ie) | (atyp2 in self.ie):
                continue

            data = np.genfromtxt(f)

            cn_not_normed = cumtrapz(data[:, 0]**2 * data[:, 1], dx=data[1, 0] - data[0, 0])
            # If data files do not contain cns:
            if data.shape[1] <= 2:
                cn = np.zeros_like(cn_not_normed)
            else:
                cn = data[:, 2]

            rdf = {'name': name.strip('.dat'),
                   'type1':atyp1, 'part1':part1,
                   'type2':atyp2, 'part2':part2,
                   'r':data[:, 0], 'gr':data[:, 1],
                   'cn_vmd':cn, 'cn':cn_not_normed,
                   'alpha':1}
            rdfs.append(rdf)

        self.rdfs = rdfs

    def fit_kb_factor(self):
        ''' Find the alpha that makes the integral
            to zero at low q, and save the alpha in the rdf dict '''

        def cost(x, *args):
            r = args[0]
            g = args[1]
            dr = r[1] - r[0]

            kb = trapz((g - 1) * w3d_extrap(r, max(r) / 2, alpha=x), dx=dr)
            return kb**2 / len(r) + 1/x

        for i, rdf in enumerate(self.rdfs):
            name  = rdf['name']
            if (rdf['part1'] == 'u') and (rdf['part2'] == 'u'):
                print(f'{name:10s}: Skipping ... ')
                continue

            r = rdf['r']
            gr = rdf['gr']
            x0 = 1
            args = (r, gr)
            int_before = trapz((gr - 1) * w3d_extrap(r, max(r) / 2, alpha=x0), dx=r[1] - r[0])
            opt = minimize(cost, x0, args)
            int_after = trapz((gr - 1) * w3d_extrap(r, max(r) / 2, alpha=opt.x[0]), dx=r[1] - r[0])
            print(f'{name:10s}: alpha: {opt.x[0]: 7.4f}, Integral Before: {int_before:7.4f}, After: {int_after:7.4f}')

            self.rdfs[i]['alpha'] = opt.x[0]

    def van_der_vegt(self, damp=None):
        ''' See: 10.1080/08927022.2017.1416114
            Correcting RDFs to account for finite-size effects. '''

        if self.all_names is None:
            self.bookkeep()
        #for i, rdf in enumerate(self.rdfs):
        self.vegt = True
        new_rdfs = []
        for sn, stoich_name in enumerate(self.all_names):  # only loop over necessary rdfs
            rdf_list = [rdf for rdf in self.rdfs if stoich_name in rdf['name']]
            if len(rdf_list) == 0:  # try oppoise
                rev_name = stoich_name.split('-')[1] + '-' +  stoich_name.split('-')[0]
                rdf_list = [rdf for rdf in self.rdfs if rev_name in rdf['name']]
                if len(rdf_list) == 0:
                    if not self.skip_missing:
                        raise RuntimeError(f'Could not find RDF for {stoich_name}')
                    else:
                        print(f'Could not find RDF for {stoich_name}, skipping...')
                        continue
                if self.verbose and (rev_name != stoich_name):
                    print(f'Using {rev_name} for {stoich_name}')
            rdf = rdf_list[0]

            new_rdfs.append(rdf)

            name = rdf['name']
            if name.count('_u') == 2:  # Don't mess with solute-solute
                continue
            r = rdf['r']
            g = rdf['gr'].copy()
            if damp:
                g -= 1
                g *= damp.damp(r)
                g += 1
            V = (4 / 3) * np.pi * r**3  # V(r), not V(r_max), see: 10.1021/ct301017q
            L3 = self.v  # Volume of box. Originally Boxlength**3 for square box.
            Nb = self.stoich[rdf['type2'] + '_' + rdf['part2']]
            rho_b = Nb / L3
            dNab = cumtrapz(4 * np.pi * r**2 * rho_b * (g - 1),  dx=(r[1] - r[0]), initial=0)

            voldiff =  1 - (V / L3)
            kd = float((rdf['type1'] == rdf['type2']) and
                       (rdf['part1'] == rdf['part2']))

            correction = 1 / ((Nb * voldiff) / (Nb * voldiff - dNab - kd))
            vdv_g = g * correction
            new_rdfs[sn]['gr'] = vdv_g
            new_rdfs[sn]['corr'] = correction

            if self.verbose:
                print(f'{name:10s}: KD: {kd:1.0f}', g[-1] - new_rdfs[sn]['gr'][-1])
        self.rdfs = new_rdfs


    def set_stoichometry(self, stoich, u_idx=None):
        ''' If the input is a dict, it assumes that the dict contains:
            {Atomtype: Number}. if not, it assumes that the input
            is an ASE-readable structure file from which it can
            figure out the stoichometry itself. '''

        if isinstance(stoich, dict):  # already formatted
            self.stoich = stoich
        elif isinstance(stoich, str):  # from xyz path
            from ase.io import read
            atoms = read(stoich)
            solu_atoms = atoms[:u_idx]
            solv_atoms = atoms[u_idx:]

            syms = solu_atoms.get_chemical_symbols()
            u_syms = [sym + '_u' for sym in syms]

            syms = solv_atoms.get_chemical_symbols()
            v_syms = [sym + '_v' for sym in syms]

            syms = u_syms + v_syms

            unique, counts = np.unique(syms, return_counts=True)
            stoich = dict(zip(unique, counts))

            # remove ignored atomtypes (yes, this is a bit ugly)
            for where in ('_u', '_v'):
                for sym in self.ie:
                    stoich.pop(sym + where, None)

            self.stoich = stoich
        elif (stoich is None) and (isinstance(self.stoich, dict)):  # already set
            if self.verbose:
                print('Stoichomestry already set')
            pass
        else:
            raise RuntimeError(f'Couldnt set stoichometry')

        # init alpha_DV based on number of solute atomtypes
        # if this is the first time since init of object
        if self.alpha_dv is None:
            self.alpha_dv = {key.strip('_u'):1
                             for key in self.stoich.keys() if '_u' in key}

    def atomic_f0(self, stoich=None):
        if stoich is None:
            stoich = self.stoich
        f0 = {}  # lol how do i dict comprehend
        for sym in stoich.keys():
            only_sym = sym.split('_')[0]
            f0[only_sym] = self.db.atomic_f0(only_sym)

        self.f0 = f0

    def atomic_f0_pt(self, stoich=None):
        from periodictable.cromermann import fxrayatq
        if stoich is None:
            stoich = self.stoich
        f0 = {}
        for sym in stoich.keys():
            only_sym = sym.split('_')[0]
            f0[only_sym] = np.array([fxrayatq(only_sym, q) for q in self.qvec])
        self.f0 = f0

    def bookkeep(self):
        ''' Check if all combinations of atomtypes in stoich are in rdfs '''
        all_names = [f'{x}-{y}' for x in self.stoich.keys() for y in self.stoich.keys()]
        for key, val in self.stoich.items():
            if val == 1:  # no key-key RDF then
                all_names.remove(key + '-' + key)

        rdf_names = [rdf['type1'] + '_' + rdf['part1'] + '-' +
                     rdf['type2'] + '_' + rdf['part2'] for rdf in self.rdfs]

        missing = [name for name in all_names if name not in rdf_names]
        if self.verbose:
            print(f'Bookkeeping messages:')
            print(f'---------------------')
            for m in missing:
                print(f'NB! Missing RDF file for the pair: {m}')

            print(f'---------------------\n')

        self.all_names = all_names

    def fit_alphadv(self, n_el=None):
        ''' Fits the alpha_dv factor in F_DV such that the cross
            scattering + DV solvent scattering sums to the correct
            low-q limit of N_l * n_l**2, where N_l is the number
            of atoms of type l, and n_l is the number of electrons
            in atom type l. '''

        if n_el is None:  # So you can put in your own dict if need be.
            n_el = {x:Atoms(x.strip('_u')).get_atomic_numbers()[0]
                    for x in self.stoich.keys() if '_u' in x}
        self.n_el = n_el

        if self.verbose:
            print('Fitting DV Scalers...')

        def cost(x, *args):
            sgr = args[0]
            sym = args[1]
            N = args[2]
            n = args[3]
            new_stoich = args[4]
            limit = N * n**2

            sgr.alpha_dv[sym] = x
            s = sgr.calculate(sgr.rdfs, new_stoich)
            cage = (s['s_c_damp'] + s['s_vdv_damp'])[0]

            return (cage - limit)**2

        for atom_type, N in self.stoich.items():
            if not '_u' in atom_type:
                continue
            # Set up dummy sgr with low q
            dummy = SGr(self.v, damp=self.damp, delta=self.delta,
                        verbose=False, ignore_elements=self.ie,
                        qvec=np.array([0.000001]))
            dummy.rdfs = self.rdfs.copy()
            dummy.stoich = self.stoich.copy()
            dummy.alpha_dv = self.alpha_dv.copy()
            elmnt = atom_type.strip('_u')

            # use stoich to only calculate cage and dv for
            # this atom_type
            new_stoich = {key:val
                          for key, val in self.stoich.items() if '_v' in key}
            new_stoich[atom_type] = N
            #if self.verbose:
            #    print(atom_type, N, n_el[atom_type], new_stoich)

            opt = minimize(cost, 1, (dummy, elmnt,
                           N, n_el[atom_type], new_stoich))
            if self.verbose:
                print(f'Element: {elmnt:2s}, Alpha_DV: {opt.x[0]:7.4f}, cost:{opt.fun:9.2e}')
            self.alpha_dv[elmnt] = opt.x[0]
            #print(self.alpha_dv)

    def fit_alphadv2(self, n_el=None, limit=None):
        ''' Use a single DV value for ALL elements and
            fit to the TOTAL SOLUTE sum limit. To avoid effectively
            messing with the stoichometry of the molecule '''

        if n_el is None:  # So you can put in your own dict if need be.
            n_el = {x:Atoms(x.strip('_u')).get_atomic_numbers()[0]
                    for x in self.stoich.keys() if '_u' in x}
        self.n_el = n_el

        if self.verbose:
            print('Fitting DV Scalers...')

        def cost(x, *args):
            sgr = args[0]
            syms = args[1]
            limit = args[2]

            for sym in syms:
                sgr.alpha_dv[sym] = x
            s = sgr.calculate(sgr.rdfs)  # fit on ALL
            cage = (s['s_c_damp'] + s['s_vdv_damp'])[0]

            return (cage - limit)**2

        # Set up dummy sgr with low q
        dummy = SGr(self.v, damp=self.damp, delta=self.delta,
                    verbose=False, ignore_elements=self.ie,
                    qvec=np.array([0.000001]))
        dummy.rdfs = self.rdfs.copy()
        dummy.stoich = self.stoich.copy()
        dummy.alpha_dv = self.alpha_dv.copy()

        if limit is None:
            limit = sum([(N * n_el[key])
                        for key, N in self.stoich.items() if '_u' in key])**2
        self.limit = limit
        syms = [key.strip('_u') for key in self.stoich.keys() if '_u' in key]

        args = (dummy, syms, limit)
        opt = minimize(cost, 1, args)

        if self.verbose:
            print(f'Fitted alpha_DV: {opt.x[0]}, cost: {opt.fun}')
        for sym in syms:
            self.alpha_dv[sym] = opt.x[0]

    def fit_alphadv3(self, n_el=None):
        ''' The limit to fit to is N * n_el**2 of solVENT atom types,
            since that is the original limit of the cage contribution. '''

        if n_el is None:  # So you can put in your own dict if need be.
            n_el = {x:Atoms(x.strip('_v')).get_atomic_numbers()[0]
                    for x in self.stoich.keys() if '_v' in x}
        self.n_el = n_el

        if self.verbose:
            print('Fitting DV Scalers...')

        def cost(x, *args):
            sgr = args[0]
            syms = args[1]
            limit = args[2]

            for sym in syms:
                sgr.alpha_dv[sym] = x
            s = sgr.calculate(sgr.rdfs)  # fit on ALL
            cage = (s['s_c_damp'] + s['s_vdv_damp'])[0]

            return (cage - limit)**2

        # Set up dummy sgr with low q
        dummy = SGr(self.v, damp=self.damp, delta=self.delta,
                    verbose=False, ignore_elements=self.ie,
                    qvec=np.array([0.000001]))
        dummy.rdfs = self.rdfs.copy()
        dummy.stoich = self.stoich.copy()
        dummy.alpha_dv = self.alpha_dv.copy()

        limit = sum([N * n_el[key]**2
                     for key, N in self.stoich.items() if '_v' in key])
        self.limit = limit
        syms = [key.strip('_u') for key in self.stoich.keys() if '_u' in key]

        args = (dummy, syms, limit)
        opt = minimize(cost, 1, args)
        if self.verbose:
            print(f'Fitted alpha_DV: {opt.x[0]}, cost: {opt.fun}')
        for sym in syms:
            self.alpha_dv[sym] = opt.x[0]

    def get_rdf(self, name):
        return [rdf for rdf in self.rdfs if name in rdf['name']][0]

    def calc_avg_fdv(self):
        ''' Calculate the F_dv based on the avg solute-solvent RDF '''
        fourpi = 4 * np.pi
        damp = self.damp
        self.f_dv = {}
        self.f_dv_damp = {}
        alpha_dv = self.alpha_dv

        if self.verbose:
            print('SAXSy AVG Solvent Displaced Volume FF:')
            print(f'---------------------')
        for atmtype1 in self.stoich.keys():
            ''' loop over AVG-solute atom types'''
            if 'U_' not in atmtype1:
                continue
            f = np.zeros_like(self.qvec)
            f_damp = np.zeros_like(self.qvec)
            for atmtype2 in self.stoich.keys():
                ''' loop over solvent atom types'''
                if '_v' not in atmtype2:
                    continue
                # Find accompanying RDF
                full_name = f'g{atmtype1}-{atmtype2}'
                rdf_lst = [rdf for rdf in self.rdfs if full_name in rdf['name']]
                if len(rdf_lst) == 0:  # try opposite
                    full_name = f'g{atmtype2}-{atmtype1}'
                    rdf_lst = [rdf for rdf in self.rdfs if full_name in rdf['name']]
                if len(rdf_lst) == 0:  # try opposite
                    if not self.skip_missing:
                        raise RuntimeError(f'Could not find RDF for {full_name}')
                    else:
                        print(f'Could not find RDF for {full_name}, skipping...')
                        continue
                rdf = rdf_lst[0]
                n_m = self.stoich[atmtype2]  # number of solvent atoms
                rho_m = n_m / self.v
                f_s = self.f0[atmtype2.strip('_v')]  # solvent atom f0
                R = rdf['r']
                g = rdf['gr']
                dr = R[1] - R[0]
                g0 = 1

                avg = 1
                if self.r_avg: # from where to take avg g0
                    avg = np.mean(g[R > self.r_avg])
                g0 *= avg

                h = np.zeros_like(self.qvec)
                h_damp = np.zeros_like(self.qvec)
                damp = self.damp

                if self.verbose:
                    print(f"{atmtype1}-{atmtype2}: n_m: {n_m:4g}, RDF name: {rdf['name']}, used solvent FF: {atmtype2}")
                # Do the integral:
                for q, qq in enumerate(self.qvec):
                    this_h = (g - g0) * (np.sinc(qq * R / np.pi)) * R**2 #/ (qq * R)
                    h[q] += trapz(this_h, dx=dr)
                    if damp is not None:
                        h_damp[q] += trapz(damp.damp(R) * this_h, dx=dr)

                    prefac = f_s[q] * rho_m
                    f[q] += prefac * h[q]
                    f_damp[q] += prefac * h_damp[q]

            dv_name = atmtype1.strip('_u')
            self.f_dv[dv_name] = alpha_dv[dv_name] * fourpi * f
            self.f_dv_damp[dv_name] = alpha_dv[dv_name] * fourpi * f_damp
        if self.verbose:
            print(f'---------------------\n')


    def calc_fdv(self):
        ''' Get f_dv for each solute atom type'''
        fourpi = 4 * np.pi
        alpha_dv = self.alpha_dv
        self.f_dv = {}
        self.f_dv_damp = {}
        self.f_dv_kb = {}
        if self.verbose:
            print('SAXSy Solvent Displaced Volume FF:')
            print(f'----------------------------------')
        for atmtype1 in self.stoich.keys():
            ''' loop over solute atom types'''
            if '_u' not in atmtype1:
                continue
            f = np.zeros_like(self.qvec)
            f_damp = np.zeros_like(self.qvec)
            f_kb = np.zeros_like(self.qvec)
            for atmtype2 in self.stoich.keys():
                ''' loop over solvent atom types'''
                if '_v' not in atmtype2:
                    continue
                # Find accompanying RDF
                full_name = f'g{atmtype1}-{atmtype2}'
                rdf_lst = [rdf for rdf in self.rdfs if full_name in rdf['name']]
                if len(rdf_lst) == 0:  # try opposite
                    full_name = f'g{atmtype2}-{atmtype1}'
                    rdf_lst = [rdf for rdf in self.rdfs if full_name in rdf['name']]
                if len(rdf_lst) == 0:  # try opposite
                    if not self.skip_missing:
                        raise RuntimeError(f'Could not find RDF for {full_name}')
                    else:
                        print(f'Could not find RDF for {full_name}, skipping...')
                        continue
                rdf = rdf_lst[0]

                n_m = self.stoich[atmtype2]  # number of solvent atoms
                rho_m = n_m / self.v
                f_s = self.f0[atmtype2.strip('_v')]  # solvent atom f0
                R = rdf['r']
                g = rdf['gr']
                dr = R[1] - R[0]
                g0 = 1

                avg = 1
                if self.r_avg: # from where to take avg g0
                    avg = np.mean(g[R > self.r_avg])
                g0 *= avg

                two_R = max(R) / 2
                alpha = 1
                if 'alpha' in rdf.keys():
                    alpha = rdf['alpha']

                if self.delta:
                    g0 = 0
                h = np.zeros_like(self.qvec)
                h_damp = np.zeros_like(self.qvec)
                h_kb = np.zeros_like(self.qvec)
                if str(self.damp) == 'Extender':  # Extend and fit the cutoff
                    damp = rdf['fitted_damp']
                else:
                    damp = self.damp
                if self.verbose:
                    print(f"{atmtype1}-{atmtype2}: n_m: {n_m:4g}, RDF name: {rdf['name']}, used solvent FF: {atmtype2}")

                # Do the integral:
                for q, qq in enumerate(self.qvec):
                    this_h = (g - g0) * (np.sinc(qq * R / np.pi)) * R**2 #/ (qq * R)
                    h[q] += trapz(this_h, dx=dr)
                    if damp is not None:
                        h_damp[q] += trapz(damp.damp(R) * this_h, dx=dr)

                    prefac = f_s[q] * rho_m
                    f[q] += prefac * h[q]
                    f_damp[q] += prefac * h_damp[q]

                    this_kb_h = (g - g0) * w3d_extrap(R, two_R, alpha) * (np.sinc(qq * R / np.pi)) #/ (qq * R))
                    h_kb[q] += trapz(this_kb_h, dx=dr)
                    f_kb[q] += prefac * h_kb[q]

            dv_name = atmtype1.strip('_u')
            self.f_dv[dv_name] = alpha_dv[dv_name] * fourpi * f
            self.f_dv_damp[dv_name] = alpha_dv[dv_name] * fourpi * f_damp
            #if self.vegt:
            #    self.f_dv_damp[dv_name] = self.f_dv[dv_name]

            self.f_dv_kb[dv_name] = alpha_dv[dv_name] * f_kb
        if self.verbose:
            print(f'----------------------------------\n')

    def first_term(self):
        s_u = np.zeros(len(self.qvec))
        s_v = np.zeros(len(self.qvec))
        s_t = np.zeros(len(self.qvec))
        s_vdv = np.zeros(len(self.qvec))  # SAXSy v
        s_vdv_damp = np.zeros(len(self.qvec))  # SAXSy v
        s_vdv_kb = np.zeros(len(self.qvec))  # SAXSyKB v

        for atype, count in self.stoich.items():
            if '_u' in atype:
                f0 = self.f0[atype.strip('_u')]
                s_u += count * f0**2
                s_vdv += count * self.f_dv[atype.strip('_u')]**2
                s_vdv_damp += count * self.f_dv_damp[atype.strip('_u')]**2
                s_vdv_kb += count * self.f_dv_kb[atype.strip('_u')]**2
            elif '_v' in atype:
                f0 = self.f0[atype.strip('_v')]
                s_v += count * f0**2
            s_t += count * f0**2


        self.debug_s_vfirst = np.copy(s_v)

        return s_u, s_v, s_t, s_vdv, s_vdv_damp, s_vdv_kb

    def second_term(self, s_u, s_v, s_t, s_vdv, s_vdv_damp, s_vdv_kb):
        s_c = np.zeros(len(self.qvec))
        s_c_damp = np.zeros(len(self.qvec))
        s_u_damp = np.copy(s_u)
        s_v_damp = np.copy(s_v)
        s_t_damp = np.copy(s_t)

        #s_v *= 0

        s_c_kb = np.zeros(len(self.qvec))
        s_v_kb = np.copy(s_v)

        s_cuse = np.ones(len(self.qvec))

        fourpi = np.pi * 4
        stoich = self.stoich
        f0 = self.f0

        if not self.damp:  # semi-safe standard damping settings if nothing set:
            damp = Damping('simple', self.rdfs[0]['r'] / 2)

        first = True
        if self.verbose:
            print(f'Second term:')
            print(f'------------')
        for sn, stoich_name in enumerate(self.all_names):  # only loop over necessary rdfs
            extra_str = ''
            # find corresponding rdf:
            rdf_list = [rdf for rdf in self.rdfs if stoich_name in rdf['name']]
            if len(rdf_list) > 2:
                raise RuntimeError('More than two RDF for the same atomtype pair was found!')
            if (len(rdf_list) == 2) and self.verbose:
                print('Warning: 2 RDFs found: '+rdf_list[0]['name'] + ' and ' + rdf_list[1]['name']
                       + f' for {stoich_name}')
            if len(rdf_list) == 0:  # didn't find it, try to reverse the name
                rev_name = stoich_name.split('-')[1] + '-' +  stoich_name.split('-')[0]
                rdf_list = [rdf for rdf in self.rdfs if rev_name in rdf['name']]

                extra_str = ''
                if self.verbose and (rev_name != stoich_name):
                    extra_str = f'Using {rev_name} for {stoich_name}'
                if len(rdf_list) == 0:
                    if not self.skip_missing:
                        raise RuntimeError(f'Could not find RDF for {stoich_name}')
                    else:
                        print(f'Could not find RDF for {stoich_name}, skipping...')
                        continue
            rdf = rdf_list[0]

            if first:
                first = False
                locator = np.zeros(len(self.all_names))

            atm1 = rdf['type1'] + '_' + rdf['part1']
            atm2 = rdf['type2'] + '_' + rdf['part2']
            n_i = stoich[atm1]
            n_j = stoich[atm2]

            R = rdf['r']
            g = rdf['gr']

            avg = 1
            if self.r_avg: # from where to take avg g0
                avg = np.mean(g[R > self.r_avg])

            if self.r_max:  # cut off the integral at this r
                mask = np.zeros(len(R), bool)
                mask[R < self.r_max] = True
                R = R[mask]
                g = g[mask]


            if str(self.damp) == 'Extender':  # Extend and fit the cutoff
                damp = rdf['fitted_damp']
            else:
                damp = self.damp

            dr = R[1] - R[0]
            two_R = max(R) / 2  # For KB
            alpha = 1
            if 'alpha' in rdf.keys():
                alpha = rdf['alpha']

            h = np.zeros_like(self.qvec)
            h_damp = np.zeros_like(self.qvec)
            h_kb = np.zeros_like(self.qvec)

            # Find out which term this RDF belongs to
            if (rdf['part1'] == 'u') & (rdf['part2'] == 'u'):
                where = 'SOLUTE'
                g_end = 0
            elif (rdf['part1'] == 'v') & (rdf['part2'] == 'v'):
                where = 'SOLVENT'
                g_end = 1
            elif ((rdf['part1'] == 'v') & (rdf['part2'] == 'u') |
                  (rdf['part1'] == 'u') & (rdf['part2'] == 'v')):
                where = 'CROSS'
                g_end = 1
            else:
                raise RuntimeError('An RDF doesnt belong to any of the 3 regions')
            if self.delta:
                g_end = 0

            g_end *= avg

            # Kronecker-delta.
            kd = float((rdf['type1'] == rdf['type2'])
                       and where != 'CROSS')

            for q, qq in enumerate(self.qvec):
                f_i = f0[rdf['type1']][q]
                f_j = f0[rdf['type2']][q]

                this_h = (g - g_end) * (np.sinc(qq * R / np.pi)) * fourpi * R**2 #/ (qq * R)
                h[q] += trapz(this_h, dx=dr)
                if damp is not None:
                    h_damp[q] += trapz(damp.damp(R) * this_h, dx=dr)

                this_kb_h = (g - g_end) * w3d_extrap(R, two_R, alpha) * (np.sinc(qq * R/ np.pi) )#/ (qq * R))
                h_kb[q] += trapz(this_kb_h, dx=dr)

                if where == 'SOLUTE':
                    locator[sn] = 0
                    prefac = n_i * (n_j - kd) * f_i * f_j / self.v
                    s_u[q] += prefac * h[q]
                    s_u_damp[q] += prefac * h_damp[q]

                    # SAXSy solvent:
                    f_l_dv = self.f_dv_damp[rdf['type1']][q]
                    f_m_dv = self.f_dv_damp[rdf['type2']][q]
                    dv_pref = n_i * (n_j - kd) * f_l_dv * f_m_dv / self.v
                    s_vdv[q] += dv_pref * h[q]
                    s_vdv_damp[q] += dv_pref * h[q]  # Solute scattering does not need damping since RDFS go to zero.
                    #s_vdv_damp[q] += dv_pref * h_damp[q]

                    # SAXSy and KBy solvent:
                    f_l_dv = self.f_dv_kb[rdf['type1']][q]
                    f_m_dv = self.f_dv_kb[rdf['type2']][q]
                    dv_pref = n_i * (n_j - kd) * f_l_dv * f_m_dv / self.v
                    s_vdv_kb[q] += dv_pref * h[q]


                elif where == 'SOLVENT':
                    locator[sn] = 1
                    prefac = n_i * (n_j - kd) * f_i * f_j / self.v
                    s_v[q] += prefac * h[q]
                    s_v_damp[q] += prefac * h_damp[q]
                    s_v_kb[q] += prefac * h_kb[q]
                    s_cuse[q] += h[q] * ((n_j - kd) / self.v)
                elif where == 'CROSS':
                    locator[sn] = 2
                    prefac = n_i * n_j * f_i * f_j / self.v
                    s_c[q] += prefac * h[q]
                    s_c_damp[q] += prefac * h_damp[q]
                    s_c_kb[q] += prefac * h_kb[q]
                else:
                    raise RuntimeError('An RDF doesnt belong to any of the 3 regions')
                # Total:
                prefac = n_i * (n_j - kd) * f_i * f_j / self.v
                s_t[q] += prefac * h[q]
                s_t_damp[q] += prefac * h_damp[q]
            if self.verbose:
                if where == 'SOLUTE':
                    print(f"{where:8s}: {atm1:4s}:{n_i:7g} - {atm2:4s}:{n_j:7g}, KD:{kd}, DV: {rdf['type1']}-{rdf['type2']} " + extra_str)
                else:
                    print(f"{where:8s}: {atm1:4s}:{n_i:7g} - {atm2:4s}:{n_j:7g}, KD:{kd}, KB_alpha: {alpha: 7.4f} " + extra_str)

        out = {'rdf':self.rdfs,
               'q': self.qvec,
               's_u': s_u,
               's_u_damp': s_u_damp,
               's_v': s_v,
               's_v_damp': s_v_damp,
               's_c': s_c,
               's_c_damp': s_c_damp,
               's_t': s_t,
               's_t_damp': s_t_damp,
               's_vdv': s_vdv,
               's_vdv_damp': s_vdv_damp,
               's_vdv_kb': s_vdv_kb,
               's_c_kb': s_c_kb,
               's_v_kb': s_v_kb,
               's_cuse': s_cuse,
               'locator': locator}
        self.out = out

        return out
