'''
THis file contains information about the experimental setup
such as jet thickness and detector

'''

x_det_length= 0.17
y_det_length= 0.17
no_of_pixels= 2032**2
energy= 18
distance_to_detector = 0.05
beam_area = 25e-6 * 25e-6

detector_thickness = 40e-6 
detector_mu = 20.5 * 1000 #18keV Value!
jet_thickness = 360e-6
jet_mu = 8.096e-01 * 0.997 * 100 # Value for 20keV

# Sample parameters, in approximation of the solvent
sample_density = 997 # kg/m3
sample_molar_mass = 0.018 #kg/Mol

# beam stop and beam pipe distance
max_distance = 0.07
min_distance = 0.03
density_gas = 1.18 # kg/m3
molar_mass_gas = 0.02897 #kg/Mol