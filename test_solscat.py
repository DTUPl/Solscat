from sol_scat import *
import matplotlib.pyplot as plt

import matplotlib 
matplotlib.rc('xtick', labelsize=20, direction='in', color ='black') 
matplotlib.rc('ytick', labelsize=20, direction='in')
#plt.rc('figure.tick_params',length=10)

matplotlib.rc('figure', facecolor='white',dpi=80,figsize=(8, 6))

font = {'family' : 'DejaVu Sans', #'normal'
        'weight' : 'bold',
        'size'   : 22}

matplotlib.rc('font', **font)
print("Fonts Loaded")


#plt.ion()


a = Simulate_scattering_image()
q, sig = a.simulate_scattering_signal()


plt.plot(q,sig, label="w/o air scattering")


a.apply_air_scattering()
q, sig = a.simulate_scattering_signal()
plt.plot(q,sig, label="with air scattering")
plt.title("Simulated scattering signal, 1e9 photons, 1 pulse")
plt.xlabel("Q [Å$^{-1}$]")
plt.ylabel("S [(e.u.)$^2$]")

plt.legend()
plt.savefig("images/scattering_signal.png")
#plt.show()
plt.clf()
image = a.scatter_photons_sample(n_photons=1e9, n_pulses = 1)
#image = a.correct_intensity(image)
plt.pcolor(image[::10])
#plt.pcolor(a.xdet[::10]*100, a.xdet[::10]*100, image[::10])
plt.title("2D Detector image")
plt.xlabel("x pos [pixel]")
plt.ylabel("y pos [pixel]")
plt.colorbar()
plt.savefig("images/2DScattering.png")
